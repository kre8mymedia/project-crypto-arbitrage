package kucoin

import(
	"log"
	"sync"
	"strings"

	kucoin "github.com/Kucoin/kucoin-go-sdk"
	broker "cryptowebsocket/broker"
)

func Creds() (*kucoin.ApiService) {
	s :=  kucoin.NewApiService( 
		kucoin.ApiBaseURIOption("https://openapi-sandbox.kucoin.com"), 
		kucoin.ApiKeyOption("62550fd32b968a0001533bd1"),
		kucoin.ApiSecretOption("ac99fd67-e4df-4eb3-9576-55c01918b73a"),
		kucoin.ApiPassPhraseOption("test1234"),
	)
	return s
}

func FetchAccounts() {
	// API key version 2.0
	s :=  Creds()
	// Without pagination
	rsp, err := s.Accounts("", "")
	if err != nil {
		// Handle error
		return
	}

	as := kucoin.AccountsModel{}
	if err := rsp.ReadData(&as); err != nil {
		// Handle error
		return
	}

	for _, a := range as {
		log.Printf("Available balance: %s %s => %s", a.Type, a.Currency, a.Available)
	}
}

func FetchPrices(wg *sync.WaitGroup) {
	defer wg.Done()
	var client = broker.FetchClient("127.0.0.1")
	s := kucoin.NewApiService( 
		kucoin.ApiBaseURIOption("https://openapi-v2.kucoin.com"), 
		// kucoin.ApiKeyOption("key"),
		// kucoin.ApiSecretOption("secret"),
		// kucoin.ApiPassPhraseOption("passphrase"),
	)

	rsp, err := s.WebSocketPublicToken()
	if err != nil {
		// Handle error
		return
	}

	tk := &kucoin.WebSocketTokenModel{}
	if err := rsp.ReadData(tk); err != nil {
		// Handle error
		return
	}

	c := s.NewWebSocketClient(tk)

	mc, ec, err := c.Connect()
	if err != nil {
		// Handle error
		return
	}

	ch1 := kucoin.NewSubscribeMessage("/market/ticker:ETH-USDT", false)
	ch2 := kucoin.NewSubscribeMessage("/market/ticker:BTC-USDT", false)
	ch3 := kucoin.NewSubscribeMessage("/market/ticker:SOL-USDT", false)
	ch4 := kucoin.NewSubscribeMessage("/market/ticker:MATIC-USDT", false)
	ch5 := kucoin.NewSubscribeMessage("/market/ticker:AVAX-USDT", false)

	if err := c.Subscribe(ch1, ch2, ch3, ch4, ch5); err != nil {
		// Handle error
		return
	}

	var i = 0
	for {
		select {
		case err := <-ec:
			c.Stop() // Stop subscribing the WebSocket feed
			log.Printf("Error: %s", err.Error())
			// Handle error
			return
		case msg := <-mc:
			// log.Printf("Received: %s", kucoin.ToJsonString(m))
			t := &kucoin.TickerLevel1Model{}
			if err := msg.ReadData(t); err != nil {
				log.Printf("Failure to read: %s", err.Error())
				return
			}
			var symbol string = msg.Topic
			symbol = strings.Replace(symbol, "/market/ticker:", "", 3)
			symbol = strings.Replace(symbol, "USDT", "USD", 3)
			var topic string = "prices/" + symbol + "/kucoin"

			broker.Publish(client, topic, t.Price)
			// log.Printf("Ticker: %s, %s, %s, %s", msg.Topic, t.Sequence, t.Price, t.Size)
			i++
			if i == 10 {
				log.Println("Subscribe ETH-BTC")
				if err = c.Subscribe(ch2); err != nil {
					log.Printf("Error: %s", err.Error())
					// Handle error
					return
				}
			}
		}
	}
}