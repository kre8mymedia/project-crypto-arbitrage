package broker

import (
	"sync"
	"fmt"
	"context"
	"strings"
	broker "cryptowebsocket/broker"
	"github.com/go-numb/go-ftx/realtime"
)

func FetchPrices(PAIRS []string, wg *sync.WaitGroup) {
	defer wg.Done()
	var client = broker.FetchClient("127.0.0.1")

    ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	ch := make(chan realtime.Response)
	go realtime.Connect(ctx, ch, []string{"ticker"}, PAIRS, nil)
	go realtime.ConnectForPrivate(ctx, ch, "<key>", "<secret>", []string{"orders", "fills"}, nil)
	
	for {
		select {
		case v := <-ch:
			switch v.Type {
			case realtime.TICKER:
				s := fmt.Sprintf("%v", v.Ticker.Last)
				var symbol string = strings.Replace(v.Symbol, "PERP", "USD", 3)
				symbol = strings.Replace(symbol, "XBT", "BTC", 3)
				topic := fmt.Sprintf("%v", "prices/" + symbol + "/ftx")
                broker.Publish(client, topic, s)
				// fmt.Printf("%s	%+v\n", v.Symbol, v.Ticker.Last)
                

			case realtime.TRADES:
				fmt.Printf("%s	%+v\n", v.Symbol, v.Trades)
				for i := range v.Trades {
					if v.Trades[i].Liquidation {
						fmt.Printf("-----------------------------%+v\n", v.Trades[i])
					}
				}

			case realtime.ORDERBOOK:
				fmt.Printf("%s	%+v\n", v.Symbol, v.Orderbook)

			case realtime.ORDERS:
				fmt.Printf("%d	%+v\n", v.Type, v.Orders)

			case realtime.FILLS:
				fmt.Printf("%d	%+v\n", v.Type, v.Fills)

			case realtime.UNDEFINED:
				fmt.Printf("UNDEFINED %s	%s\n", v.Symbol, v.Results.Error())
			}
		}
	}
}