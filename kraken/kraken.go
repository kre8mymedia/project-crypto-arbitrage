package kraken

import (
	"os"
	"sync"
	"os/signal"
	"syscall"
	"strings"

	log "github.com/sirupsen/logrus"

	ws "github.com/aopoltorzhicky/go_kraken/websocket"

	broker "cryptowebsocket/broker"
)

func FetchPrices(PAIRS []string, wg *sync.WaitGroup) {
	defer wg.Done()
	var client = broker.FetchClient("127.0.0.1")

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	kraken := ws.NewKraken(ws.ProdBaseURL)
	if err := kraken.Connect(); err != nil {
		log.Fatalf("Error connecting to web socket: %s", err.Error())
	}

	// subscribe to BTCUSD`s ticker
	if err := kraken.SubscribeTicker(PAIRS); err != nil {
		log.Fatalf("SubscribeTicker error: %s", err.Error())
	}

	for {
		select {
		case <-signals:
			log.Warn("Stopping...")
			if err := kraken.Close(); err != nil {
				log.Fatal(err)
			}
			return
		case update := <-kraken.Listen():
			switch data := update.Data.(type) {
			case ws.TickerUpdate:
				var symbol string = strings.Replace(update.Pair, "XBT", "BTC", 3)
				pair := strings.Replace(symbol, "/", "-", 3)
				topic := "prices/" + pair + "/kraken"
				broker.Publish(client, topic, data.Ask.Price.String())

				// log.Printf("----Ticker of %s----", update.Pair)
				// log.Printf("Ask: %s with %s", data.Ask.Price.String(), data.Ask.Volume.String())
				// log.Printf("Bid: %s with %s", data.Bid.Price.String(), data.Bid.Volume.String())
				// log.Printf("Open today: %s | Open last 24 hours: %s", data.Open.Today.String(), data.Open.Last24.String())
			default:
			}
		}
	}
}