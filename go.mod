module cryptowebsocket

go 1.16

require (
	github.com/Kucoin/kucoin-go-sdk v1.2.12
	github.com/aopoltorzhicky/go_kraken/websocket v0.1.8
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/go-numb/go-ftx v0.0.0-20220404235717-18debbaf2924
	github.com/gorilla/websocket v1.5.0
	github.com/preichenberger/go-coinbasepro/v2 v2.1.0
	github.com/sirupsen/logrus v1.8.1
)
