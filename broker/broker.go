package broker

import (
	"fmt"
    "math/rand"
	"time"
	mqtt "github.com/eclipse/paho.mqtt.golang"
)
    

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
    fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
    fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
    fmt.Printf("Connect lost: %v", err)
}

func Publish(client mqtt.Client, topic string, price string) {
    // text := fmt.Sprintf("Message %d", i)
    fmt.Printf("%v %v\n", topic, price)
    token := client.Publish(topic, 0, false, price)
    token.Wait()
    time.Sleep(time.Millisecond)
}

func FetchClient(host string) mqtt.Client {
    var broker = host
    var port = 1883
    opts := mqtt.NewClientOptions()
    opts.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
    s := fmt.Sprintf("%v", rand.Float64())
    opts.SetClientID(s)
    opts.SetKeepAlive(2 * time.Second)
    opts.SetPingTimeout(1 * time.Second)
    // opts.SetUsername("emqx")
    // opts.SetPassword("public")
    opts.SetDefaultPublishHandler(messagePubHandler)
    opts.OnConnect = connectHandler
    opts.OnConnectionLost = connectLostHandler
    client := mqtt.NewClient(opts)
    if token := client.Connect(); token.Wait() && token.Error() != nil {
        panic(token.Error())
    }
	return client
}