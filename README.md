### Table of Contents
- [Download MQTT Explorer](http://mqtt-explorer.com/)

# Run to support build services [Redis, Postgres, RabbitMQ]
<pre>
docker-compose up --build
</pre>

- MQTT Explorer Credentials
    - Host: 127.0.0.1
    - Port: 1883
    - Username: (not required)
    - Password: (not required)
- MQTT Explorer Credentials
    - Host: 127.0.0.1
    - Port: 5050
    - Username: test@test.com 
    - Password: admin

### Run exchange price feeds
<pre>
go run .
</pre>

