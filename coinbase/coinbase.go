package coinbase

import(
	"fmt"
	"sync"

	gdax "github.com/preichenberger/go-coinbasepro/v2"
    ws "github.com/gorilla/websocket"
	broker "cryptowebsocket/broker"
)

func FetchPrices(PAIRS []string, wg *sync.WaitGroup) {
	defer wg.Done()
	var client = broker.FetchClient("127.0.0.1")
	var wsDialer ws.Dialer
	wsConn, _, err := wsDialer.Dial("wss://ws-feed.pro.coinbase.com", nil)
	if err != nil {
		println(err.Error())
	}

	subscribe := gdax.Message{
		Type:      "subscribe",
		Channels: []gdax.MessageChannel{
			{
				Name: "ticker",
				ProductIds: PAIRS,
			},
		},
	}
	if err := wsConn.WriteJSON(subscribe); err != nil {
		println(err.Error())
	}

	for true {
		message := gdax.Message{}
		if err := wsConn.ReadJSON(&message); err != nil {
			println(err.Error())
			break
		}
		topic := fmt.Sprintf("%v", "prices/" + message.ProductID + "/coinbase")
		if (message.ProductID != "") {
			broker.Publish(client, topic, message.Price)
		}
	}
}