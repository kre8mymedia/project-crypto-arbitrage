package main

import (
    "fmt"
    "sync"
    "os"
    "os/signal"
    "syscall"

    ftx "cryptowebsocket/ftx"
    kraken "cryptowebsocket/kraken"
    coinbase "cryptowebsocket/coinbase"
    kucoin "cryptowebsocket/kucoin"
)

func cleanup() {
    fmt.Println("cleanup")
}

func main() {
    c := make(chan os.Signal)
    signal.Notify(c, os.Interrupt, syscall.SIGTERM)

    go func() {
        <-c
        cleanup()
        os.Exit(1)
    }()

    for {
        var wg sync.WaitGroup

        wg.Add(3)
        go ftx.FetchPrices([]string{"BTC-PERP", "ETH-PERP", "SOL-PERP", "MATIC-PERP", "AVAX-PERP"}, &wg)
        go kraken.FetchPrices([]string{"BTC/USD", "ETH/USD","SOL/USD", "MATIC/USD", "AVAX/USD"}, &wg)
        go coinbase.FetchPrices([]string{"BTC-USD", "ETH-USD", "SOL-USD", "MATIC-USD", "AVAX-USD"}, &wg)
        go kucoin.FetchPrices(&wg)
        fmt.Println("Waiting for goroutines to finish...")
        wg.Wait()
        fmt.Println("Done!") // or runtime.Gosched() or similar per @misterbee
    }
}